import express from 'express';

let app = express();

const checkTime = (req, res, next) => {
    const date = new Date(Date.now());
    const day = date.getDay();
    const hour = date.getHours();
    

    if(day > 5  || hour < 9 || hour > 17){
        return res.render('non-working-hours');
    }
    next();
}

app.use(express.static('public'));
app.use(checkTime);


app.set('view engine', 'ejs');
app.set('views', './views');


app.get('/', (req, res) => {
    res.render('homepage');
})

app.get('/contact', (req, res) => {
    res.render('contact');
})

app.get('/services', (req, res) => {
    res.render('services');
})


app.listen(8080);